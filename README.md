## Prerequisites:
* IDE (Netbeans 8.1) 
* JDK 1.8
* Maven 3.*

## External libraries:
* springboot-elasticseach
* springboot-starter test

## Install elasticseach
* Download elasticseach at https://www.elastic.co/downloads/elasticsearch (I'm using elasticseach 2.4.0)
* Extract elasticseach
* Go to elasticseach folder, change some elasticseach setting in "config/elasticsearch.yml" as you want to match with project
  Ex: cluster.name: qle-cluster, node.name: qle-node, index.number_of_shards: 1, index.number_of_replicas: 0
* Run command "bin/elasticseach"
  In case root user, we should run command "bin/elasticsearch -Des.insecure.allow.root=true"

## Step to run service
1. Clone the source code from public repository https://quanglth@bitbucket.org/quanglth/springbootrest.git
2. Go to project directory on your computer, and run the command "mvn clean install"
3. Run command "java -jar target/SpringBootRest-1.0.0.jar" to start REST service
