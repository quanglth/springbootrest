/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  quanglt
 * Created: Feb 25, 2019
 */

CREATE TABLE `user` (
  `id` bigint(20) AUTO_INCREMENT PRIMARY KEY,
  `age` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `salary` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `notes` (
  `id` bigint(20) AUTO_INCREMENT PRIMARY KEY,
  `title` varchar(255) NOT NULL,
  `content` varchar(255) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `course` (
  `id` bigint(20) AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE course_rating (
    user_id bigint(20) NOT NULL,
    course_id bigint(20) NOT NULL,
    rating int NOT NULL,
    CONSTRAINT PK_Course_Rating PRIMARY KEY (user_id, course_id)
);


/**
    ===================Foreign Keys=================
*/
ALTER TABLE notes
  ADD CONSTRAINT FK_User_Note FOREIGN KEY (user_id) REFERENCES user (id);

ALTER TABLE course_rating
  ADD CONSTRAINT FK_User_Course_Rating_UID FOREIGN KEY (user_id) REFERENCES user (id);

ALTER TABLE course_rating
  ADD CONSTRAINT FK_User_Course_Rating_CID FOREIGN KEY (course_id) REFERENCES course (id);