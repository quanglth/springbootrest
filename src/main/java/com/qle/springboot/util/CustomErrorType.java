/*
 * Copyright (C) 2017 QuangLe.
 */
package com.qle.springboot.util;

/**
 *
 * @author quangle
 */
public class CustomErrorType {
    
    private String errorMessage;
 
    public CustomErrorType(String errorMessage){
        this.errorMessage = errorMessage;
    }
 
    public String getErrorMessage() {
        return errorMessage;
    }
}
