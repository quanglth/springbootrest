/*
 * Copyright (C) 2017 QuangLe.
 */
package com.qle.springboot.service;

import com.qle.springboot.model.Book;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

/**
 *
 * @author quangle
 */
public interface BookService {
    
    Book save(Book book);

    void delete(Book book);
    
    void deleteAll();

    Book findOne(String id);

    Iterable<Book> findAll();

    Page<Book> findByAuthor(String author, PageRequest pageRequest);

    List<Book> findByTitle(String title);
}
