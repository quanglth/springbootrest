/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qle.springboot.service;

import com.qle.springboot.exception.NoCourseRatingFoundException;
import com.qle.springboot.model.CourseRating;
import com.qle.springboot.model.CourseRatingKey;
import com.qle.springboot.repository.CourseRatingRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

/**
 *
 * @author quanglt
 */
@Service("courseRatingService")
public class CourseRatingServiceImpl implements CourseRatingService {

    @Autowired
    private CourseRatingRepository courseRatingRepository;

    @Override
    public List<CourseRating> findAll() {
        return courseRatingRepository.findAll();
    }

    @Override
    public List<CourseRating> findAll(Sort sort) {
        return courseRatingRepository.findAll(sort);
    }

    @Override
    public List<CourseRating> findAll(Iterable<CourseRatingKey> itrbl) {
        return courseRatingRepository.findAll(itrbl);
    }

    @Override
    public CourseRating save(CourseRating courseRating) {
        return courseRatingRepository.save(courseRating);
    }

    @Override
    public CourseRating findOne(CourseRatingKey id) {
        CourseRating course = courseRatingRepository.findOne(id);
        if(course == null) {
            throw new NoCourseRatingFoundException(id);
        }
        
        return course;
    }

    @Override
    public void delete(CourseRatingKey id) {
        CourseRating course = findOne(id);
        courseRatingRepository.delete(course);
    }

    @Override
    public CourseRating updateCourseRating(CourseRatingKey id, CourseRating courseRating) {
        CourseRating existedCourseRating = findOne(id);
        existedCourseRating.setRating(courseRating.getRating());
        return save(existedCourseRating);
    }
}
