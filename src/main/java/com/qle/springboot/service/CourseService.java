/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qle.springboot.service;

import com.qle.springboot.model.Course;
import java.util.List;
import org.springframework.data.domain.Sort;

/**
 *
 * @author quanglt
 */
public interface CourseService {

    public List<Course> findAll();

    public List<Course> findAll(Sort sort);

    public List<Course> findAll(Iterable<Long> itrbl);

    public Course save(Course course);
    
    public Course findOne(Long id);
    
    public void delete(Long id);
    
    public Course updateCourse(Long id, Course course);
}
