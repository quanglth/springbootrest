package com.qle.springboot.service;

/*
 * Copyright (C) 2017 QuangLe.
 */
import com.qle.springboot.model.Note;
import com.qle.springboot.model.User;
import java.util.List;
import org.springframework.data.domain.Sort;

/**
 *
 * @author quangle
 */
public interface UserService {
    
    User findById(long id);
     
    User findByName(String name);
     
    User saveUser(User user);
     
    User updateUser(long id, User user);
     
    void deleteUserById(long id);
 
    public List<User> findAll();

    public List<User> findAll(Sort sort);

    public List<User> findAll(Iterable<Long> itrbl);
}
