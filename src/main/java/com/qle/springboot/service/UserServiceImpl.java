/*
 * Copyright (C) 2017 QuangLe.
 */
package com.qle.springboot.service;

import com.qle.springboot.exception.NoUserFoundException;
import com.qle.springboot.model.User;
import com.qle.springboot.repository.UserRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

/**
 *
 * @author quangle
 */
@Service("userService")
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public User findById(long id) {
        User user = userRepository.findOne(id);
        if (user == null) {
            throw new NoUserFoundException();
        }

        return user;
    }

    @Override
    public User findByName(String name) {
        throw new UnsupportedOperationException("This method does not support yet");
    }

    @Override
    public User saveUser(User user) {
        return userRepository.save(user);
    }

    @Override
    public User updateUser(long id, User user) {
        User updatedUser = findById(id);
        updatedUser.setAge(user.getAge());
        updatedUser.setName(user.getName());
        updatedUser.setSalary(user.getSalary());
        return userRepository.save(updatedUser);
    }

    @Override
    public void deleteUserById(long id) {
        User user = findById(id);
        userRepository.delete(user);
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public List<User> findAll(Sort sort) {
        return userRepository.findAll(sort);
    }

    @Override
    public List<User> findAll(Iterable<Long> itrbl) {
        return userRepository.findAll(itrbl);
    }
}
