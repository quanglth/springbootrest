/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qle.springboot.service;

import com.qle.springboot.exception.NoCourseFoundException;
import com.qle.springboot.model.Course;
import com.qle.springboot.repository.CourseRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

/**
 *
 * @author quanglt
 */
@Service("courseService")
public class CourseServiceImpl implements CourseService {

    @Autowired
    private CourseRepository courseRepository;

    @Override
    public List<Course> findAll() {
        return courseRepository.findAll();
    }

    @Override
    public List<Course> findAll(Sort sort) {
        return courseRepository.findAll(sort);
    }

    @Override
    public List<Course> findAll(Iterable<Long> itrbl) {
        return courseRepository.findAll(itrbl);
    }

    @Override
    public Course save(Course course) {
        return courseRepository.save(course);
    }

    @Override
    public Course findOne(Long id) {
        Course course = courseRepository.findOne(id);
        if(course == null) {
            throw new NoCourseFoundException(id);
        }
        
        return course;
    }

    @Override
    public void delete(Long id) {
        Course course = findOne(id);
        courseRepository.delete(course);
    }

    @Override
    public Course updateCourse(Long id, Course course) {
        Course existedCourse = findOne(id);
        existedCourse.setName(course.getName());
        return save(existedCourse);
    }
}
