/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qle.springboot.service;

import com.qle.springboot.model.Note;
import java.util.List;
import org.springframework.data.domain.Sort;

/**
 *
 * @author quanglt
 */
public interface NoteService {

    public List<Note> findAll();

    public List<Note> findAll(Sort sort);

    public List<Note> findAll(Iterable<Long> itrbl);

    public Note save(Note note);
    
    public Note findOne(Long id);
    
    public void delete(Note id);
    
    public void delete(Long note);
}
