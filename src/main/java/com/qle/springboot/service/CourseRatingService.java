/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qle.springboot.service;

import com.qle.springboot.model.CourseRating;
import com.qle.springboot.model.CourseRatingKey;
import java.util.List;
import org.springframework.data.domain.Sort;

/**
 *
 * @author quanglt
 */
public interface CourseRatingService {

    public List<CourseRating> findAll();

    public List<CourseRating> findAll(Sort sort);

    public List<CourseRating> findAll(Iterable<CourseRatingKey> itrbl);

    public CourseRating save(CourseRating courseRating);
    
    public CourseRating findOne(CourseRatingKey id);
    
    public void delete(CourseRatingKey id);
    
    public CourseRating updateCourseRating(CourseRatingKey id, CourseRating courseRating);
}
