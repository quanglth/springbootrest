/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qle.springboot.service;

import com.qle.springboot.model.Note;
import com.qle.springboot.repository.NoteRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

/**
 *
 * @author quanglt
 */
@Service("noteService")
public class NoteServiceImpl implements NoteService {

    @Autowired
    private NoteRepository noteRepository;

    @Override
    public List<Note> findAll() {
        return noteRepository.findAll();
    }

    @Override
    public List<Note> findAll(Sort sort) {
        return noteRepository.findAll(sort);
    }

    @Override
    public List<Note> findAll(Iterable<Long> itrbl) {
        return noteRepository.findAll(itrbl);
    }

    @Override
    public Note save(Note note) {
        return noteRepository.save(note);
    }

    @Override
    public Note findOne(Long id) {
        return noteRepository.findOne(id);
    }

    @Override
    public void delete(Long id) {
        noteRepository.delete(id);
    }

    @Override
    public void delete(Note note) {
        noteRepository.delete(note);
    }
}
