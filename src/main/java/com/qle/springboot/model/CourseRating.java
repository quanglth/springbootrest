/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qle.springboot.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 *
 * @author quanglt
 */
@Entity
@Table(name = "course_rating")
@EntityListeners(AuditingEntityListener.class)
public class CourseRating implements Serializable {

    @EmbeddedId
    private CourseRatingKey id;

    @ManyToOne
    @MapsId(value = "user_id")
    @JoinColumn(name = "user_id")
    @JsonBackReference(value = "userCourseRatings")
    private User user;

    @ManyToOne
    @MapsId(value = "course_id")
    @JoinColumn(name = "course_id")
    @JsonBackReference(value = "courseRatings")
    private Course course;
    
    private int rating;

    public CourseRatingKey getId() {
        return id;
    }

    public void setId(CourseRatingKey id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }
    
    
}
