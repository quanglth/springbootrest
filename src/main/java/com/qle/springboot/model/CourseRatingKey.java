/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qle.springboot.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author quanglt
 */
@Embeddable
public class CourseRatingKey implements Serializable {
    
    @Column(name = "user_id")
    private long userId;
    
    @Column(name = "course_id")
    private long courseId;
    
    public CourseRatingKey() {
        super();
    }

    public CourseRatingKey(long userId, long courseId) {
        this.userId = userId;
        this.courseId = courseId;
    }
    
    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getCourseId() {
        return courseId;
    }

    public void setCourseId(long courseId) {
        this.courseId = courseId;
    }

    @Override
    public String toString() {
        return String.format("UserID[%s]-CourseID[%s]", userId, courseId);
    }
    
}
