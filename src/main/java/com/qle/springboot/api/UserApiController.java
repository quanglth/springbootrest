/*
 * Copyright (C) 2017 QuangLe.
 */

package com.qle.springboot.api;

import com.qle.springboot.model.Course;
import com.qle.springboot.model.CourseRating;
import com.qle.springboot.model.CourseRatingKey;
import com.qle.springboot.model.User;
import com.qle.springboot.service.CourseRatingService;
import com.qle.springboot.service.CourseService;
import com.qle.springboot.service.UserService;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
/**
 *
 * @author quangle
 */
@RestController
@RequestMapping(value = "/api/user", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class UserApiController {
    
    public static final Logger logger = LoggerFactory.getLogger(UserApiController.class);
    
    @Autowired
    private UserService userService; //Service which will do all data retrieval/manipulation work
    
    @Autowired
    private CourseService courseService;
    
    @Autowired
    private CourseRatingService courseRatingService;
 
    // -------------------Retrieve All Users---------------------------------------------
 
    @RequestMapping(value = "", method = RequestMethod.GET)
    public List<User> getAllUsers() {
        return userService.findAll();
    }
 
    // -------------------Retrieve Single User------------------------------------------
 
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public User getUser(@PathVariable("id") long id) {
        logger.info("Fetching User with id {}", id);
        return userService.findById(id);
    }
 
    // -------------------Create a User-------------------------------------------
 
    @RequestMapping(value = "", method = RequestMethod.POST)
    public User createUser(@RequestBody User user) {
        logger.info("Creating User : {}", user);
        return userService.saveUser(user);
    }
 
    // ------------------- Update a User ------------------------------------------------
 
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public User updateUser(@PathVariable("id") long id, @RequestBody User user) {
        logger.info("Updating User with id {}", id);
        return userService.updateUser(id, user);
    }
 
    // ------------------- Delete a User-----------------------------------------
 
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteUser(@PathVariable("id") long id) {
        logger.info("Fetching & Deleting User with id {}", id);
 
        userService.deleteUserById(id);
        return ResponseEntity.ok().build();
    }
    
    @PostMapping("/rating/{userId}/{courseId}/{ratingValue}")
    public CourseRating rateCourse(@PathVariable("userId") long userId,
            @PathVariable("courseId") long courseId,
            @PathVariable("ratingValue") int ratingValue) {
        Course course = courseService.findOne(courseId);
        User user = userService.findById(userId);
        CourseRatingKey courseRatingKey = new CourseRatingKey(userId, courseId);
        CourseRating courseRating = new CourseRating();
        courseRating.setId(courseRatingKey);
        courseRating.setUser(user);
        courseRating.setCourse(course);
        courseRating.setRating(ratingValue);
        return courseRatingService.save(courseRating);
    }
}
