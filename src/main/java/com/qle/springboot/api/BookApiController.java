/*
 * Copyright (C) 2017 QuangLe.
 */

package com.qle.springboot.api;

import com.google.common.collect.Lists;
import com.qle.springboot.model.Book;
import com.qle.springboot.service.BookService;
import com.qle.springboot.util.CustomErrorType;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;
/**
 *
 * @author quangle
 */
@RestController
@RequestMapping("/api/book")
public class BookApiController {
    
    public static final Logger logger = LoggerFactory.getLogger(BookApiController.class);
    
    @Autowired
    BookService bookService; //Service which will do all data retrieval/manipulation work
 
    // -------------------Retrieve All Books---------------------------------------------
 
    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<List<Book>> listAllBooks() {
        Iterable<Book> book = bookService.findAll();
        List<Book> books = Lists.newArrayList(book);
        if (books.isEmpty()) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(books, HttpStatus.OK);
    }
 
    // -------------------Retrieve Single Book------------------------------------------
 
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getBook(@PathVariable("id") String id) {
        logger.info("Fetching Book with id {}", id);
        Book book = bookService.findOne(id);
        if (book == null) {
            logger.error("Book with id {} not found.", id);
            return new ResponseEntity(new CustomErrorType("Book with id " + id 
                    + " not found"), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(book, HttpStatus.OK);
    }
 
    // -------------------Create a Book-------------------------------------------
 
    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity<?> createBook(@RequestBody Book book, UriComponentsBuilder ucBuilder) {
        logger.info("Creating Book : {}", book);
 
        if (bookService.findOne(book.getId()) != null) {
            logger.error("Unable to create. A Book with name {} already exist", book.getTitle());
            return new ResponseEntity(new CustomErrorType("Unable to create. A Book with name " + 
            book.getTitle()+ " already exist."),HttpStatus.CONFLICT);
        }
        bookService.save(book);
 
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/api/book/{id}").buildAndExpand(book.getId()).toUri());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }
 
    // ------------------- Update a Book ------------------------------------------------
 
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateBook(@PathVariable("id") String id, @RequestBody Book book) {
        logger.info("Updating Book with id {}", id);
 
        Book currentBook = bookService.findOne(id);
 
        if (currentBook == null) {
            logger.error("Unable to update. Book with id {} not found.", id);
            return new ResponseEntity(new CustomErrorType("Unable to upate. Book with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);
        }
 
        currentBook.setTitle(book.getTitle());
        currentBook.setAuthor(book.getAuthor());
        currentBook.setReleaseDate(book.getReleaseDate());
 
        bookService.save(currentBook);
        return new ResponseEntity<>(currentBook, HttpStatus.OK);
    }
 
    // ------------------- Delete a Book-----------------------------------------
 
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteBook(@PathVariable("id") String id) {
        logger.info("Fetching & Deleting Book with id {}", id);
 
        Book book = bookService.findOne(id);
        if (book == null) {
            logger.error("Unable to delete. Book with id {} not found.", id);
            return new ResponseEntity(new CustomErrorType("Unable to delete. Book with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);
        }
        bookService.delete(book);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
 
    // ------------------- Delete All Books-----------------------------
 
    @RequestMapping(value = "/", method = RequestMethod.DELETE)
    public ResponseEntity<Book> deleteAllBooks() {
        logger.info("Deleting All Books");
 
        bookService.deleteAll();
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
