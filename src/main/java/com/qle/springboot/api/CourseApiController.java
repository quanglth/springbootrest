/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qle.springboot.api;

import com.qle.springboot.model.Course;
import com.qle.springboot.service.CourseService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author quanglt
 */
@RestController
@RequestMapping(value = "/api/course", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class CourseApiController {

    @Autowired
    private CourseService courseService;

    // Get All Courses
    @GetMapping("") // We can use @RequestMapping(value = "", method = RequestMethod.GET)
    public List<Course> getAllCourses() {
        return courseService.findAll();
    }

    // Create a new Course
    @PostMapping("")
    public Course createCourse(@Valid @RequestBody Course course) {
        return courseService.save(course);
    }

    // Get a Single Course
    @GetMapping("/{id}")
    public Course getCourse(@PathVariable(value = "id") Long id) {
        return courseService.findOne(id);
    }

    // Update a Course
    @PutMapping("/{id}")
    public Course updateCourse(@PathVariable(value = "id") Long id, @Valid @RequestBody Course course) {
        return courseService.updateCourse(id, course);
    }

    // Delete a Course
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteCourse(@PathVariable(value = "id") Long id) {
        courseService.delete(id);
        return ResponseEntity.ok().build();
    }
}
