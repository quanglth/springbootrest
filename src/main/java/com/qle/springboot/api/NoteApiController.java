/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qle.springboot.api;

import com.qle.springboot.exception.NoNoteFoundException;
import com.qle.springboot.model.Note;
import com.qle.springboot.model.User;
import com.qle.springboot.service.NoteService;
import com.qle.springboot.service.UserService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author quanglt
 */
@RestController
@RequestMapping(value = "/api/note", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class NoteApiController {

    @Autowired
    private NoteService noteService;
    
    @Autowired
    private UserService userService;

    // Get All Notes
    @GetMapping("") // We can use @RequestMapping(value = "", method = RequestMethod.GET)
    public List<Note> getAllNotes() {
        return noteService.findAll();
    }

    // Create a new Note
    @PostMapping("/{userId}")
    public Note createNote(@PathVariable(value = "userId") Long userId, @Valid @RequestBody Note note) {
        User user = userService.findById(userId);
        note.setUser(user);
        return noteService.save(note);
    }

    // Get a Single Note
    @GetMapping("/{id}")
    public Note getNote(@PathVariable(value = "id") Long id) {
        Note note = noteService.findOne(id);
        if (note == null) {
            throw new NoNoteFoundException(id);
        }

        return note;
    }

    // Update a Note
    @PutMapping("/{id}/{userId}")
    public Note updateNote(@PathVariable(value = "id") Long id, @PathVariable(value = "userId") Long userId, @Valid @RequestBody Note note) {
        Note existedNote = getNote(id);
        User user = userService.findById(userId);
        existedNote.setTitle(note.getTitle());
        existedNote.setContent(note.getContent());
        existedNote.setUser(user);
        return noteService.save(existedNote);
    }

    // Delete a Note
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteNote(@PathVariable(value = "id") Long id) {
        Note existedNote = getNote(id);
        noteService.delete(existedNote);
        return ResponseEntity.ok().build();
    }
}
