/*
 * Copyright (C) 2017 QuangLe.
 */
package com.qle.springboot.repository;

import com.qle.springboot.model.Book;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 *
 * @author quangle
 */
public interface BookRepository extends ElasticsearchRepository<Book, String> {
    
    Page<Book> findByAuthor(String author, Pageable pageable);

    List<Book> findByTitle(String title);
}
