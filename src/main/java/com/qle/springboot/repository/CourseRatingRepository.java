/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qle.springboot.repository;

import com.qle.springboot.model.CourseRating;
import com.qle.springboot.model.CourseRatingKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author quanglt
 */
@Repository
public interface CourseRatingRepository extends JpaRepository<CourseRating, CourseRatingKey> {
    
}
