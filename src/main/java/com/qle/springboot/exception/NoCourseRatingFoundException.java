/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qle.springboot.exception;

import com.qle.springboot.model.CourseRatingKey;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author quanglt
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class NoCourseRatingFoundException extends RuntimeException {
    
    public NoCourseRatingFoundException() {
        
    }
    
    public NoCourseRatingFoundException(CourseRatingKey id) {
        super(String.format("The course rating with ID %s is not found", id.toString()));
    }
}
