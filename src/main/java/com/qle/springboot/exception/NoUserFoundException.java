/*
 * Copyright (C) 2017 QuangLe.
 */
package com.qle.springboot.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author quangle
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class NoUserFoundException extends RuntimeException {
    
    public NoUserFoundException(){
        
    }
    
    public NoUserFoundException(long id) {
        super(String.format("User with ID = %s not found", id));
    }
    
    public NoUserFoundException(String message) {
        super(message);
    }
    
    public NoUserFoundException(Throwable cause) {
        super(cause);
    }
    
    public NoUserFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
