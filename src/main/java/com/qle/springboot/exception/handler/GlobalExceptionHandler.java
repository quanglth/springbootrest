/*
 * Copyright (C) 2017 QuangLe.
 */
package com.qle.springboot.exception.handler;

import com.qle.springboot.exception.NoUserFoundException;
import com.qle.springboot.model.ErrorMessage;
import java.util.Locale;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author quangle
 */
@ControllerAdvice  
@RestController
public class GlobalExceptionHandler {
    
    private static final int NO_USER_FOUND_ERRORCODE = 408;
    private static final String NO_USER_FOUND_MSG_KEY = "no.user.found";
    
    @Autowired
    private MessageSource messageSource;
    
    @ResponseStatus(HttpStatus.BAD_REQUEST)  
    @ExceptionHandler(value = NoUserFoundException.class)  
    public ErrorMessage handleNoUserFoundException(NoUserFoundException e){
        String message = e.getMessage();
        if(message == null || message.isEmpty()) {
            message = messageSource.getMessage(NO_USER_FOUND_MSG_KEY, null, LocaleContextHolder.getLocale());
        }
        return new ErrorMessage(HttpStatus.BAD_REQUEST.value(), NO_USER_FOUND_ERRORCODE, message);
    }
}
