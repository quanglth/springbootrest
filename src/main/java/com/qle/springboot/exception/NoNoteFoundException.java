/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qle.springboot.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author quanglt
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class NoNoteFoundException extends RuntimeException {
    
    public NoNoteFoundException() {
        
    }
    
    public NoNoteFoundException(Long id) {
        super(String.format("The note with ID %s is not found", id));
    }
}
